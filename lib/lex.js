/* lexical grammar */
%lex
%%

\s+                   /* skip whitespace */

//language
"very"                  return 'var'
"is"                       return '=' //this should be interesting
"such"                  return 'function'
"much"                 return 'PARAMSTART' // Will need to add some logic
"wow"                   return '}'
"wow&"                return '})'
"plz"                       return 'STARTFUNCALL'
"rly"                       return 'if'
"but"                      return 'else'
"notrly"                  return 'NEGATEDIF'
"so"                        return 'require'
"as"                        return 'VAREQ'
"dose"                    return '.'
"trained"                return 'use strict'



//operators

"not"                     return '!=='
"is"                        return '==='
"and"                    return '&&'
"or"                       return '||'
"next"                   return ';' //Maybe implement auto semicolons? have an indented language....
"as"                      return '='
"more"                 return '+='
"less"                   return '-='
"lots"                    return '*='
"few"                    return '/='
"bigger"               return ">"
"smaller"             return "<"
"biggerish"          return ">="
"smallerish"        return "<="


//standard objects:

"console.loge"      return "console.log"
"dogeument "        return 'document'
"windoge"               return 'window'



<<EOF>>               return 'EOF'
 
/lex



